import React, { useContext, useState } from "react";
import { Button, Form, Message } from "semantic-ui-react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { AuthContext } from "../context/auth";
import { useForm } from "../util/hooks";

function Login(props) {
  const context = useContext(AuthContext);

  const [errors, setErorrs] = useState({});

  const { onChange, onSubmit, values } = useForm(loginUserRegister, {
    username: "",
    password: ""
  });

  const [loginUser, { loading }] = useMutation(LOGIN_USER, {
    update(
      _,
      {
        data: { login: userData }
      }
    ) {
      console.log(userData);
      context.login(userData);
      props.history.push("/");
    },
    onError(err) {
      setErorrs(err.graphQLErrors[0].extensions.exception.errors);
    },
    variables: values
  });

  function loginUserRegister() {
    // hoist addUser() to be used in UseForm util/hooks
    loginUser();
  }

  return (
    <div className="ui text container">
      <Form
        onSubmit={onSubmit}
        noValidate
        className={loading ? "loading ui text container" : "ui text container"}
      >
        <h1> Login User </h1>
        <Form.Group>
          <Form.Input
            label="Username"
            placeholder="Username .."
            name="username"
            type="text"
            error={errors.username ? true : false}
            value={values.username}
            onChange={onChange}
          />
          <Form.Input
            label="Password"
            placeholder="Password .."
            name="password"
            error={errors.password ? true : false}
            value={values.password}
            onChange={onChange}
            type="password"
          />
        </Form.Group>
        <Button type="submit" primary>
          Login
        </Button>
      </Form>
      {Object.keys(errors).length > 0 && ( //if there is an error show message
        <Message negative className="ui text container">
          <Message.Header>We're sorry there's Error</Message.Header>
          <ul className="list">
            {Object.values(errors).map(value => (
              <li key={value}>{value}</li>
            ))}
          </ul>
        </Message>
      )}
    </div>
  );
}

// GraphQl mutation

const LOGIN_USER = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      id
      email
      username
      createdAt
      token
    }
  }
`;

export default Login;
